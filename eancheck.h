#include <iostream.h>
#include <stdlib.h>
#include <string.h>

int ec_checkean(char inputean[])
{
 int trip = 0;

 for (int c = 0; c < strlen(inputean); c++)
 {
   if ((inputean[c] < 48) || (inputean[c] > 57))
    trip++;
 }

 if (trip == 0)
  {
   // the variable for the numbers passed to the function
   int digits[strlen(inputean)];
   
   for (int i=0; i < strlen(inputean); i++)
    {
     // this makes each digit of the EAN a string (with terminating
     // character) so that atoi can work its magic on it.
     char convert[2];
    
     convert[0] = inputean[i];
    
     convert[1] = '\0';
  
     // converts the char (as sent as argument) into a number.
     digits[i] = atoi(convert);
    }

     // the total of all the "odd" numbers.
     int oaccumulator = 0;
     // the total of all the "even" numbers.
     int eaccumulator = 0;
  
     for (int o=0; o<strlen(inputean)-1; o=o+2)
     {
      oaccumulator = oaccumulator + digits[o];
     }
 
     for (int e=1; e<strlen(inputean)-1; e=e+2)
   {
    eaccumulator = eaccumulator + digits[e];
   }
 
   short int cdigit;
  
   cdigit = 10 - (((eaccumulator * 3) + oaccumulator) % 10);
   if (cdigit == 10) cdigit = 0;
  
   if (cdigit == digits[strlen(inputean)-1])
   {
    return 0;
   } else {
    return 1;
  }
 } else {
    return 3;
 }
}

int ec_gencd(char inputean[])
{
 int trip = 0;

 for (int c = 0; c < strlen(inputean); c++)
 {
   if ((inputean[c] < 48) || (inputean[c] > 57))
    trip++;
 }

 if (trip == 0)
  {
   // the variable for the numbers passed to the function
   int digits[strlen(inputean)];
   
   for (int i=0; i < strlen(inputean); i++)
    {
     // this makes each digit of the EAN a string (with terminating
     // character) so that atoi can work its magic on it.
     char convert[2];
    
     convert[0] = inputean[i];
    
     convert[1] = '\0';
  
     // converts the char (as sent as argument) into a number.
     digits[i] = atoi(convert);
    }

     // the total of all the "odd" numbers.
     int oaccumulator = 0;
     // the total of all the "even" numbers.
     int eaccumulator = 0;
  
     for (int o=0; o<strlen(inputean); o=o+2)
     {
      oaccumulator = oaccumulator + digits[o];
     }
 
     for (int e=1; e<strlen(inputean); e=e+2)
   {
    eaccumulator = eaccumulator + digits[e];
   }
 
   short int cdigit;
  
   cdigit = 10 - (((eaccumulator * 3) + oaccumulator) % 10);
   if (cdigit == 10) cdigit = 0;
   return cdigit;
 } else {
    return 10;
 }
}
