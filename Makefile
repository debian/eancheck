PREFIX=/usr

all: eancheck

eancheck: eancheck.o
	g++ -o $@ $<

eancheck.o: eancheck.cpp
	g++ -Wno-deprecated -c -o $@ $<

eancheck.1.gz:
	gzip -c -9 eancheck.1 > eancheck.1.gz

install: eancheck eancheck.1.gz
	install -c -D -o root -g root -m 755 eancheck $(PREFIX)/bin/eancheck
	install -c -D -o root -g root -m 644 eancheck.h $(PREFIX)/include/eancheck.h
	install -c -D -o root -g root -m 644 README $(PREFIX)/share/doc/eancheck/README
	install -c -D -o root -g root -m 644 eancheck.1.gz $(PREFIX)/share/man/man1/eancheck.1.gz

uninstall:
	rm $(PREFIX)/bin/eancheck
	rm $(PREFIX)/include/eancheck.h
	rm -rf $(PREFIX)/share/doc/eancheck
	rm $(PREFIX)/share/man/man1/eancheck.1.gz

clean:
	-rm eancheck
	-rm eancheck.o
	-rm eancheck.1.gz
