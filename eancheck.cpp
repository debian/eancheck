/*
LICENSE INFORMATION: I don't give a flying f*ck what you do with this
program or its code. Really. I don't care. Not one bit. Use it in
a program, change the code, redistribute the code (changed or not
changed...whatever). You're allowed to do anything and everything
with it. Maybe Windows Vista will include a "Check EAN
check digit" function, based on this. I very much doubt it.

This is just a little program I hacked up in a few minutes. It takes in
an EAN code as a command line argument and prints to STDOUT whether it 
is valid or not. It does this by performing a checksum on the supplied
EAN.

It can also check:

 - UPC codes (12 digits), just preface them with a zero
 - Four digit codes (PLUs) which some supermarkets with older tills
   use on their till systems to ring up products like bread, fruit, veg...
   basically any loose product. Eancheck can validate them, no prefixing
   needed.

If you want to use the eancheck functions in a program, I invite you to
use eancheck.h in your programs. It provides two functions, one to check
check digits and another to generate them for a given EAN.
*/

#include <iostream.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
 
 if (argc == 1)
 {
  cout << "Eancheck by Joe Baldwin" << endl;
  cout << "A simple program to check whether an EAN code (barcode number)"
       << " is acceptable by validating its check digit." << endl;
  cout << "Usage: eancheck 0123456789012" << endl;
  cout << "To check a UPC (12 digit) code, prefix it with a zero" << endl;
  cout << "4 digit till codes work without prefixing." << endl
       << endl;
 } else {
  int trip = 0;
  
  for (int c = 0; c < strlen(argv[1]); c++)
  {
   if ((argv[1][c] < 48) || (argv[1][c] > 57))
    trip++;
  }
  
  if (trip == 0)
  {
   // the variable for the numbers input via the command line.
   int digits[strlen(argv[1])];
  
   for (int i=0; i < strlen(argv[1]); i++)
    {
     // this makes each digit of the EAN a string (with terminating
     // character) so that atoi can work its magic on it.
     char convert[2];
    
     convert[0] = argv[1][i];
    
     convert[1] = '\0';
  
     // converts the char (as input on the command line) into a number.
     digits[i] = atoi(convert);
    }
 
   // the total of all the "odd" numbers.
   int oaccumulator = 0;
   // the total of all the "even" numbers.
   int eaccumulator = 0;
  
   for (int o=0; o<strlen(argv[1])-1; o=o+2)
   {
    oaccumulator = oaccumulator + digits[o];
   }
 
   for (int e=1; e<strlen(argv[1])-1; e=e+2)
   {
    eaccumulator = eaccumulator + digits[e];
   }
 
   short int cdigit;
  
   cdigit = 10 - (((eaccumulator * 3) + oaccumulator) % 10);
 
   if (cdigit == 10) cdigit = 0;
  
   if (cdigit == digits[strlen(argv[1])-1])
   {
    cout << "Check digit correct" << endl;
    return 0;
   } else {
    cout << "Check digit fail" << endl;
    return 1;
   }
  } else {
   cout << "Invalid input. Numbers only please." << endl;
   return 2;
  }
 }
}
